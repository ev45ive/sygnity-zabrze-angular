import {
  Directive,
  Input,
  OnInit,
  HostBinding,
  OnChanges,
  SimpleChanges
} from "@angular/core";

@Directive({
  selector: "[appImgLoad]"
})
export class ImgLoadDirective implements OnInit, OnChanges {
  @Input()
  src;

  @HostBinding("src")
  actualSrc = "assets/Spinner.gif";

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    const img = new Image();
    img.onload = () => {
      this.actualSrc = img.src;
    };
    img.src = this.src;
  }
}
