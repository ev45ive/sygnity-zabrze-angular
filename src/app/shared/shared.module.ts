import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CardComponent } from "./card/card.component";
import { HighlightDirective } from "./highlight.directive";
import { ImgLoadDirective } from "./img-load.directive";
import { MusicProviderDirective } from "./music-provider.directive";

@NgModule({
  declarations: [
    CardComponent,
    HighlightDirective,
    ImgLoadDirective,
    MusicProviderDirective
  ],
  imports: [CommonModule],
  exports: [
    CardComponent,
    HighlightDirective,
    ImgLoadDirective,
    MusicProviderDirective
  ]
})
export class SharedModule {}
