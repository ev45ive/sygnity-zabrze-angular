import { Directive, ElementRef, Input, SimpleChanges, HostBinding, HostListener } from "@angular/core";

@Directive({
  selector: "[appHighlight]",
  // host:{
  //   '[style.border-color]':'hover? color : "" ',
  //   '(mouseenter)':' hover = true ',
  //   '(mouseleave)':' hover = false ',
  // }
})
export class HighlightDirective {

  @Input("appHighlight")
  color

  @HostBinding('style.border-color')
  get activeColor(){
    return this.active? this.color : ''
  };

  // @HostBinding('class.active')
  active = false

  @HostListener('mouseenter',['$event.target'])
  activate(target){
    this.active = true 
  }


  @HostListener('mouseleave')
  deactivate(){
    this.active = false 
  }

  constructor(private elem: ElementRef) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    // console.log("NO hej!", this.color);
    // this.elem.nativeElement.style.color = this.color;
  }
}


// console.log(HighlightDirective)