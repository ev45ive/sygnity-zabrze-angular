import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from "@angular/core";
import { Playlist } from "src/app/models/Playlist";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"]
})
export class PlaylistFormComponent {
  @ViewChild("nameRef", { static: true })
  nameRef;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    console.log(this.nameRef);
  }

  @Input()
  playlist: Playlist;

  @Output()
  cancel = new EventEmitter();

  @Output()
  save = new EventEmitter();

  onCancel() {
    this.cancel.emit();
  }

  onSave(draft:Partial<Playlist>) {
    const playlist:Playlist = {
      ...this.playlist,
      ...draft,
    }

    this.save.emit(playlist);
  }
}

// type Partial<T> = {
//   [key in keyof T]?:  T[key]
// };
// Pick<Playlist,'name'|'favorite'|'color'>
// type PartialPlaylist = Partial<Playlist>
