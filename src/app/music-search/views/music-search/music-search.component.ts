import { Component, SkipSelf } from "@angular/core";
import { MusicSearchService } from "../../services/music-search.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, filter, tap } from "rxjs/operators";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.scss"]
  // viewProviders:[MusicSearchService]
})
export class MusicSearchComponent {
  message = "";

  query$ = this.service.query$;
  albums$ = this.service.getAlbums();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {

    this.route.queryParamMap
      .pipe(
        map(params => params.get("q")),
        filter(q => !!q),
        tap(q => this.service.search(q))
      )
      .subscribe();
  }

  ngOnInit() {}

  search(query: string) {
    this.router.navigate([], {
      queryParams: { q: query },
      replaceUrl:true,
      relativeTo: this.route
    });
  }
}

// this.route.queryParamMap.subscribe(queryParamMap => {
//   const q = queryParamMap.get("q");
//   if (q) {
//     this.search(q);
//   }
// });
