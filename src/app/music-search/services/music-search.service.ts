import { Injectable, Inject } from "@angular/core";
import { Album } from "src/app/models/album";
import { SEARCH_API_URL } from "../tokens";
import { HttpClient } from "@angular/common/http";
import { AlbumsResponse } from "../../models/Album";
import { BehaviorSubject, EMPTY, Subject } from "rxjs";
import {
  map,
  exhaustMap,
  mergeAll,
  concatAll,
  switchAll,
  exhaust,
  switchMap,
  catchError
} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class MusicSearchService {
  constructor(
    @Inject(SEARCH_API_URL) private api_url,
    private http: HttpClient
  ) {
    this.queryChanges
      .pipe(
        map(query => ({ type: "album", q: query })),
        switchMap(params =>
          this.http.get<AlbumsResponse>(this.api_url, { params }).pipe(
            catchError(err => {
              this.errors.next(err);
              return EMPTY;
            })
          )
        ),
        map(resp => resp.albums.items)
      )
      .subscribe(this.albumChanges);

    (window as any).subject = this.albumChanges;
  }

  results: Album[] = [
    {
      id: "123",
      name: "Album example",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Album example",
      images: [
        {
          url: "https://www.placecage.com/gif/400/400"
        }
      ]
    },
    {
      id: "123",
      name: "Album example",
      images: [
        {
          url: "https://www.placecage.com/gif/500/500"
        }
      ]
    },
    {
      id: "123",
      name: "Album example",
      images: [
        {
          url: "https://www.placecage.com/gif/600/600"
        }
      ]
    }
  ];

  private errors = new Subject<Error>();
  private queryChanges = new BehaviorSubject<string>("batman");

  public query$ = this.queryChanges.asObservable();
  private albumChanges = new BehaviorSubject<Album[]>(this.results);

  search(query: string) {
    this.queryChanges.next(query);
  }

  getAlbums() {
    return this.albumChanges.asObservable();

    // return this.http
    //   .get<AlbumsResponse>(this.api_url, {
    //     params: {
    //       query: "batman",
    //       type: "album"
    //     }
    //   })
    //   .pipe(map(resp => resp.albums.items));
  }
}
