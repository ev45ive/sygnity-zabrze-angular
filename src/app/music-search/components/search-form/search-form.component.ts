import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  mapTo,
  map,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer, combineLatest } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  // https://www.neutrinoapi.com/api/bad-word-filter/
  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const badword = "batman";
    const hasError =
      "string" == typeof control.value && control.value.includes(badword);

    return hasError
      ? {
          censor: { badword }
        }
      : null;
  };

  asyncCensor = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    // return this.http.get('/validation',{paramt}).pipe(map(res => errors))

    return Observable.create((observer: Observer<ValidationErrors | null>) => {
      // onSubscribe:
      const timer = setTimeout(() => {
        const validationResult = this.censor(control);
        observer.next(validationResult);
        observer.complete();
      }, 1000);

      // onUnsubscribe:
      return () => {
        clearTimeout(timer);
      };
    });
  };

  @Input()
  set query(q) {
    (this.queryForm.get("query") as FormControl).setValue(q, {
      emitEvent: false,
    });
  }

  // ngOnChanges(changes) {
  //   this.queryForm.get("query").setValue(changes.query.currentValue, {
  //     emitEvent: false
  //   });
  // }

  queryForm = new FormGroup({
    query: new FormControl(
      "",
      [
        Validators.required,
        Validators.minLength(3)
        // this.censor
      ],
      [this.asyncCensor]
    )
  });

  constructor() {
    (window as any).form = this.queryForm;
  }

  ngOnInit() {
    const field = this.queryForm.get("query");

    const valueChanges = field.valueChanges;

    const validChanges = field.statusChanges.pipe(
      filter(s => s === "VALID"),
      mapTo(true)
    );

    const searchValues = validChanges.pipe(
      withLatestFrom(valueChanges),
      map(([valid, value]) => value)
    );

    searchValues
      .pipe
      // debounceTime(400),
      // filter(query => query.length >= 3),
      // distinctUntilChanged()
      ()
      .subscribe({
        next: query => this.search(query)
      });
  }

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);
  }
}
