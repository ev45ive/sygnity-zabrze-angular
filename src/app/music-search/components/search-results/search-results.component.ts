import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../../../models/Album';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {

  @Input() results: Album[]

  constructor() { }

  ngOnInit() {
  }

  trackFn(index,item){
    return item.id
  }
}
