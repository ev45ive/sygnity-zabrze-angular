import { InjectionToken } from '@angular/core';

export const SEARCH_API_URL 
= new InjectionToken('Token for API URL for Music Search ')