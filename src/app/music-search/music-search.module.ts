import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicSearchRoutingModule } from "./music-search-routing.module";
import { MusicSearchComponent } from "./views/music-search/music-search.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { SearchResultsComponent } from "./components/search-results/search-results.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "src/environments/environment";
import { SEARCH_API_URL } from "./tokens";
import { MusicSearchService } from "./services/music-search.service";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    MusicSearchRoutingModule,
    SharedModule
  ],
  exports: [MusicSearchComponent],
  providers: [
    {
      provide: SEARCH_API_URL,
      useValue: environment.api_url
    }
    // {
    //   provide: MusicSearchService,
    //   useFactory(url){
    //     return new MusicSearchService(url)
    //   },
    //   deps:[SEARCH_API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService,
    //   // deps: [HOLIDAY_DEMO_SEARCH_API_URL]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService,
    //   // deps: [HOLIDAY_DEMO_SEARCH_API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService
    // },
    // MusicSearchService
  ]
})
export class MusicSearchModule {}
