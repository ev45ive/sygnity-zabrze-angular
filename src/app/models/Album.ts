// https://github.com/manifoldco/swagger-to-ts/
// http://www.jsontots.com/

export interface Entity {
  id: string;
  name: string;
}
export interface Album extends Entity {
  artists?: Artist[];
  images: ImageObject[];
}

export interface Artist extends Entity {}
export interface ImageObject {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
