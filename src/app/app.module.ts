import { ApplicationRef } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from './playlists/playlists.module';
import { MusicSearchModule } from './music-search/music-search.module';
import { SecurityModule } from './security/security.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule, 
    AppRoutingModule, 
    PlaylistsModule, 
    MusicSearchModule,
    SecurityModule,
    SharedModule
  ],
  providers: [],
  // entryComponents:[AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private app:ApplicationRef){}
  // ngDoBootstrap(){
  //   this.app.bootstrap(AppHEaderComponent, document.getElementsByTagName('app-root')[0])
  //   this.app.bootstrap(AppBodyComponent, document.getElementsByTagName('app-root')[0])
  //   this.app.bootstrap(AppFooterComponent, document.getElementsByTagName('app-root')[0])
  // }
}
