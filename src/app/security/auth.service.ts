import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

// placki@placki.com
// ******

export class AuthConfig {
  auth_url: string; //  https://accounts.spotify.com/authorize
  client_id: string;
  redirect_uri: string;
  response_type: "token";
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) {
    this.token = sessionStorage.getItem("token");

    if (!this.token && location.hash) {

      const params = new HttpParams({
        fromString: location.hash
      });

      const token = params.get("#access_token");
      if (token) {
        location.hash = ''
        this.token = token;
        sessionStorage.setItem("token", token);
      }
    }
  }

  authorize() {
    sessionStorage.removeItem('token')
        
    const { client_id, redirect_uri, response_type, auth_url } = this.config;
    const params = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type
      }
    }).toString();

    const url = `${auth_url}?${params}`;
    location.href = (url);
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
