import { NgModule, Inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthService, AuthConfig } from "./auth.service";
import { environment } from "../../environments/environment";
import { HTTP_INTERCEPTORS, HttpClientXsrfModule } from "@angular/common/http";
import { AuthInterceptorService } from "./auth-interceptor.service";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientXsrfModule.disable()
    // HttpClientXsrfModule.withOptions({
    //   cookieName:'CIASTECZKOFY_POTFOR',
    //   headerName:'X-CIASTECZKO'
    // })
  ],
  providers: [
    {
      provide: AuthConfig,
      useValue: environment.authConfig
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ]
})
export class SecurityModule {
  constructor(
    private auth: AuthService // @Inject(HTTP_INTERCEPTORS) interceptors
  ) {
    // debugger;
    this.auth.getToken();
  }
}
