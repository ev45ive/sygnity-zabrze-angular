import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { AuthService } from "./auth.service";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // req.url.match(/spotyfiy|youtube)
    
    const authRequest = req.clone({
      setHeaders: {
        Authorization: "Bearer " + this.auth.getToken()
      }
    });

    return next.handle(authRequest).pipe(
      catchError((err, caught) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.auth.authorize();
          }
          return throwError(Error(err.error.error.message));
        }
        return throwError(err);
      })
    );
  }
}
